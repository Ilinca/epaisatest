//
//  ApiMethods.m
//  ePaisaTest
//
//  Created by Ilinca Ispas on 22/09/2016.
//  Copyright © 2016 Ilinca Ispas. All rights reserved.
//

#import "ApiMethods.h"

@implementation ApiMethods

+(id)sharedApiManager {
    static ApiMethods *sharedMyManager = nil;
    if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    return sharedMyManager;
}

-(id)init {
    if(self = [super init]) {
        self.responseDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSMutableDictionary)downloadSongsListWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler {
    NSLog(@"doing stuff here");
    
    NSUrl *url = [NSUrl urlWithString: @"https://itunes.apple.com/search?term=Michael+jackson"];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]  initWithURL:url];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession]
                                      dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if (error) {
                                              NSLog(@"smth went wrong");
                                          }
                                          else{
                                              completionHandler(data, response, error);
                                          }
                                          
                                      }];
    [dataTask resume];

}


@end
