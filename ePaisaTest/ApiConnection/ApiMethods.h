//
//  ApiMethods.h
//  ePaisaTest
//
//  Created by Ilinca Ispas on 22/09/2016.
//  Copyright © 2016 Ilinca Ispas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiMethods : NSObject

@property (nonatomic, strong) NSMutableDictionary *responseDictionary;

+(id)sharedApiManager;

- (NSMutableDictionary)downloadSongsListWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error)) completionHandler;


@end
