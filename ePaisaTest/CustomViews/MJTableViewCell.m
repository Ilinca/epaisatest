//
//  MJTableViewCell.m
//  ePaisaTest
//
//  Created by Ilinca Ispas on 26/09/2016.
//  Copyright © 2016 Ilinca Ispas. All rights reserved.
//

#import "MJTableViewCell.h"

@implementation MJTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
