//
//  main.m
//  ePaisaTest
//
//  Created by Ilinca Ispas on 22/09/2016.
//  Copyright © 2016 Ilinca Ispas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
